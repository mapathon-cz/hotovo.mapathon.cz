# hotovo.mapathon.cz
Web page with the list of mapped statistics for mapathons in CZ & SK.
The `master` branch is deployed to [https://hotovo.mapathon.cz/][]

[https://hotovo.mapathon.cz/]: https://hotovo.mapathon.cz/

The list is taken from the [Missing Maps CZ & SK trello][].

[Missing Maps CZ & SK trello]: https://trello.com/b/8Vo5QWU6/missing-maps-cz-sk

## License
The project is published under [GNU AGPLv3 license][].

[GNU AGPLv3 license]: ./LICENSE

# Contribute
Report bugs and discuss features in the [Issues][], send patches via [Merge
Requests][].

[issues]: https://gitlab.com/mapathon-cz/hotovo.mapathon.cz/-/issues
[merge requests]: https://gitlab.com/mapathon-cz/hotovo.mapathon.cz/-/merge_requests

Write [great git commit messages][]:

1. Separate subject from body with a blank line.
2. Limit the subject line to 50 characters.
3. Capitalize the subject line.
4. Do not end the subject line with a period.
5. Use the imperative mood in the subject line.
6. Wrap the body at 72 characters.
7. Use the body to explain what and why vs. how.

[great git commit messages]: https://chris.beams.io/posts/git-commit/
