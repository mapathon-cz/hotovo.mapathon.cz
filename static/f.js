var LOAD_ALL = false;
var DONE = {};
var TSTART = new Date();
var STATS = {};
var mapathon_heading = {};

/*! Return Trello server endpoint.
*/
function trello_ep(e)
{
    return "https://api.trello.com/1"+e;
}

/*! Return OpenStreetMap server endpoint.
*/
function osm_ep(e)
{
    return "https://api.openstreetmap.org/api/0.6"+e;
}

/*! Update div's innerHTML.
*/
function up(i, w)
{
    document.getElementById(i).innerHTML = w;
}

/*! Append div's innerHTML.
*/
function app(i, w)
{
    document.getElementById(i).innerHTML += w;
}

/*! Send AJAJ request

\see https://en.wikipedia.org/wiki/JSON#AJAJ

\param cb Callback function with obj parameter.
\param url Request URL.
*/
function get_ajaj(cb, url)
{
    var http_request = new XMLHttpRequest();
    http_request.open("GET", url, true);
    http_request.responseType = "json";
    http_request.onreadystatechange = function() {
        var done = 4, ok = 200;
        if (http_request.readyState === done && http_request.status === ok) {
            cb(http_request.response);
        }
    };
    http_request.send(null);
}
function get_ajaj_sync(cb, url)
{
    var http_request = new XMLHttpRequest();
    http_request.open("GET", url, false);
    http_request.responseType = "xml";
    http_request.onreadystatechange = function() {
        var done = 4, ok = 200;
        if (http_request.readyState === done && http_request.status === ok) {
            cb(http_request.response);
        }
    };
    http_request.send(null);
}

// Get info from Trello Missing Maps CZ & SK.
function get_mapathons_all()
{
    LOAD_ALL = true;
    get_mapathons();
}
function get_mapathons()
{
    get_ajaj(
        function(list) {
            var mapathons = [];
            for (var c in list) {
                if (list[c]["due"] == null)
                    continue;
                if (!list[c]["name"].includes("|"))
                    continue;
                mapathons.push(list[c]);
            }
            mapathons.sort(function(a, b){
                return new Date(b["due"]) - new Date(a["due"]);
            });
            for (var m in mapathons) {
                var mh = "";
                mh += mapathons[m]["name"];
                mh += " (";
                mh += (new Date(mapathons[m]["due"]))
                    .toLocaleString("cs-CZ")
                    .slice(0,-3);
                mh += ")";
                mapathon_heading[mapathons[m]["id"]] = mh;
                var ih = "";
                ih += "<div";
                    ih += " id='" + mapathons[m]["id"] + "'";
                    ih += " class='m'";
                    ih += " onclick='get_comments(";
                        ih += "\"" + mapathons[m]["id"] + "\"";
                    ih += ")'";
                ih += ">";
                ih += mh;
                ih += "</div>";
                app("main", ih);
                if (LOAD_ALL)
                    get_comments(mapathons[m]["id"]);
            }
            if (LOAD_ALL)
                LOAD_ALL = false;
        },
        trello_ep("/boards/5952cabd9f36ed8565925685/cards?filter=all"),
    );
}

function get_comments(cid)
{
    DONE[cid] = {};
    STATS[cid] = {};
    up(cid, mapathon_heading[cid] + "<br /><br />Načítám ...");
    get_ajaj(
        function(list) {
            var tstart = new Date(list["due"]);
            var tend = new Date(list["due"]);
            tend.setHours(tstart.getHours() + 4);
            var comments = [];
            for (var c in list["actions"]) {
                if (list["actions"][c]["type"] == "commentCard")
                    comments.push(list["actions"][c]["data"]["text"]);
            }
            var tasks = [];
            for (var c in comments) {
                tasks = comments[c].match(/#[0-9]{4,5}([^0-9]|$)/g);
                if (tasks != null && tasks.length > 0)
                    break;
            }
            for (var t in tasks)
                tasks[t] = tasks[t].trim().slice(1);
            if (tasks == null)
                return print_stats(cid);
            if (tasks.length <= 0)
                return print_stats(cid);
            for (var t in tasks)
                DONE[cid][tasks[t]] = false;
            for (var t in tasks) {
                STATS[cid][tasks[t]] = {
                    "created": {
                        "buildings": 0,
                    },
                    "modified": {
                        "buildings": 0,
                    },
                };
            }
            for (var t in tasks)
                get_changesets(cid, tasks[t], tstart, tend);
        },
        trello_ep("/cards/" + cid + "?actions=commentCard"),
    );
}

function get_changesets(cid, hpid, tstart, tend)
{
    var list = [];
    while (tstart < tend) {
        var loading = document.getElementById("loading");
        loading.innerHTML = "Načítám projekt " + hpid;
        var et = ((new Date()) - TSTART) / 1000;
        loading.innerHTML += " (uběhlo " + et + "s)";
        get_ajaj_sync(
            function(osm) {
                var parser = new DOMParser();
                var xml = parser.parseFromString(osm, "text/xml");
                try {
                    var changesets = xml.getElementsByTagName("changeset");
                    tend = new Date(
                        changesets
                        .item(changesets.length - 1)
                        .getAttribute("created_at")
                    );
                    for (var c in changesets) {
                        var tags = changesets[c].getElementsByTagName("tag");
                        for (var t in tags) {
                            try {
                                if (tags[t].getAttribute("k") != "comment")
                                    continue;
                                var v = tags[t].getAttribute("v");
                                var ret = "project-" + hpid;
                                var re = new RegExp(ret);
                                if (v.match(re)) {
                                    var s = download_stats(
                                        changesets[c].getAttribute("id")
                                    );
                                    STATS[cid][hpid]["created"]["buildings"] +=
                                        s["created"]["buildings"]
                                    ;
                                    STATS[cid][hpid]["modified"]["buildings"] +=
                                        s["modified"]["buildings"]
                                    ;
                                }
                            } catch(e) {
                            }
                        }
                    }
                } catch(e) {
                }
            },
            osm_ep(
                "/changesets?time="
                + tstart.toISOString()
                + ","
                + tend.toISOString()
            ),
        );
    }
    DONE[cid][hpid] = true;
    var done = true;
    for (var i in DONE) {
        for (var j in DONE[i]) {
            if (DONE[i][j] == false)
                done = false;
        }
    }
    if (done) {
        var et = ((new Date()) - TSTART) / 1000;
        var loading = document.getElementById("loading");
        loading.innerHTML = "Načteno za " + et + "s.";
    }
    var done_cid = true;
    for (var j in DONE[cid]) {
        if (DONE[cid][j] == false)
            done_cid = false;
    }
    if (done_cid)
        print_stats(cid);
}

function download_stats(chid)
{
    var stats = {};
    get_ajaj_sync(
        function(osm) {
            var parser = new DOMParser();
            var xml = parser.parseFromString(osm, "text/xml");
            try {
                var created = xml.getElementsByTagName("create");
                stats["created"] = {
                    "buildings": 0,
                };
                var modified = xml.getElementsByTagName("modify");
                stats["modified"] = {
                    "buildings": 0,
                };
                for (var c in created) {
                    try {
                        var ways = created[c].getElementsByTagName("way");
                        for (var w in ways) {
                            var tags = ways[w].getElementsByTagName("tag");
                            if (tags == null || tags.length == 0)
                                continue;
                            if (tags[0].getAttribute("k") == "building")
                                stats["created"]["buildings"] += 1;
                        }
                    } catch(e) {
                    }
                }
                for (var m in modified) {
                    try {
                        var ways = modified[m].getElementsByTagName("way");
                        for (var w in ways) {
                            var tags = ways[w].getElementsByTagName("tag");
                            if (tags == null || tags.length == 0)
                                continue;
                            if (tags[0].getAttribute("k") == "building")
                                stats["modified"]["buildings"] += 1;
                        }
                    } catch(e) {
                    }
                }
            } catch(e) {
            }
        },
        osm_ep("/changeset/" + chid + "/download"),
    );
    return stats;
}

function print_stats(cid)
{
    var ih = "";
    ih += mapathon_heading[cid];
    ih += "<ul>";
    var sum = {
        "created": {
            "buildings": 0,
        },
        "modified": {
            "buildings": 0,
        },
    };
    for (var j in STATS[cid]) {
        try {
            var cb = STATS[cid][j]["created"]["buildings"];
            var mb = STATS[cid][j]["modified"]["buildings"];
        } catch(e) {
            var cb = 0;
            var mb = 0;
        }
        sum["created"]["buildings"] += cb;
        sum["modified"]["buildings"] += mb;
        ih += "<li>";
        ih += "Projekt " + j;
        ih += "<ul>";
        ih += "<li>Vytvořené budovy: " + cb + "</li>";
        ih += "<li>Upravené budovy: " + mb + "</li>";
        ih += "</ul>";
        ih += "</li>";
    }
    ih += "<li>Celkem</li>";
    ih += "<ul>";
    ih += "<li>Vytvořené budovy: ";
        ih += sum["created"]["buildings"];
    ih += "</li>";
    ih += "<li>Upravené budovy: ";
        ih += sum["modified"]["buildings"];
    ih += "</li>";
    ih += "</ul>";
    ih += "</ul>";
    up(cid, ih);
}
